// 1) DOM – це деревоподібне відтворення веб-сайту, у вигляді об'єктів, вкладених один в одного, потрібне для
// коректного відображення сайт та внесення змін на сторінки за допомогою JS.
// 2) innerHtml повертає весь текст, включаючи HTML-теги, що містяться в елементі, а innerText повертає
// весь текст, який міститься в елементі та у всіх його дочірніх елементах у вигляді просто тексту.
// 3) getElementsByClassName() - звернення до всіх елементів з однаковою назвою классу;
// getElementById() - звернення до елементу за унікальнім id;
// getElementsByName() - пошук серед елементів з атрибутом name, повертає список всіх елементів,
// чий атрибут name задовольняє запиту;
// getElementsByTagName() - звернення до всіх елементів з однаковою назвою тегу;
// querySelector() - шукає та повертає перший елемент, що задовольняє даному CSS-селектору;
// querySelectorAll() - повертає всі елементи, що задовольняють даномму CSS-селектору.
// краще використовувати querySelector() i querySelectorAll()

// 1.Знайти всі параграфи на сторінці та встановити колір фону #ff0000
let paragraphs = document.getElementsByTagName('p');
for (let i=0; i < paragraphs.length; i++) {
    paragraphs[i].style.background = '#ff0000';
}

// 2.Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
let elemOptionList = document.getElementById('optionsList');
console.log(elemOptionList);

let parentElem = document.getElementById('optionsList').parentNode;
console.log(parentElem);

let childElements = document.getElementById('optionsList').childNodes;
console.log(childElements);
childElements.forEach(childNode => {
    console.log(`child node: name - ${childNode.nodeName}; type: ${childNode.nodeType}`);
})

// 3.Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph
// let testParagraph = document.getElementsByClassName('testParagraph');
// console.log(testParagraph)
// в завданні знайти по класу testParagraph, але такого класу немає, тільки id

let testParagraph = document.getElementById('testParagraph');
console.log(testParagraph);
testParagraph.innerText = 'This is a paragraph';
console.log(testParagraph);

// 4.Отримати елементи , вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
let mainHeader = document.querySelector('.main-header');
console.log(mainHeader);
let mainHeaderChildren = mainHeader.childNodes;
console.log(mainHeaderChildren);
mainHeaderChildren.forEach(childElement => {
    childElement.className = 'nav-item'
});


// 5.Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
let sectionTitles = document.querySelectorAll('.section-title');
console.log(sectionTitles);
sectionTitles.forEach(sectionTitle => {
    sectionTitle.classList.remove('section-title');
    console.log(sectionTitles.className)
})



